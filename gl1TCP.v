`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ICEPP
// Engineer: Ikuo Otani
// 
// Create Date:    3/12/2013 
// Design Name: 
// Module Name:    glink mouth1 to TCP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module gl1TCP
(
input		wire			CLK125P,
input		wire			CLK125N,

output	wire			ETH_RESET_B,
input		wire			ETH_CRS,
input		wire			ETH_COL,
output	wire			ETH_GTXCLK,
input		wire			ETH_TXCLK,
output	wire	[7:0]	ETH_TXD,
output	wire			ETH_TXEN,
output	wire			ETH_TXER,
input		wire			ETH_RXCLK,
input		wire	[7:0]	ETH_RXD,
input		wire			ETH_RXDV,
input		wire			ETH_RXER,

output	wire			ETH_CLKIN,
//input		wire			ETH_INTRPT_B,
output	wire			ETH_MDC,
inout		wire			ETH_MDIO,

//input		wire			ETH_DPLX_B,
//input		wire			ETH_ACT,
//input		wire			ETH_10M,
//input		wire			ETH_100M_B,
input		wire			ETH_1000M_B,

output	wire			PROM_CS,
output	wire			PROM_SK,
output	wire			PROM_DI,
input		wire			PROM_DO,

input		wire				FPGA_CLK,
input		wire				FPGA_RESET_B,
output	wire				FPGA_STATE,
//input		wire				FPGA_WSTR_B,
//input		wire				FPGA_RSTR_B,
output	wire	[3:0]		FPGA_LED,
output	wire	[15:0]	FPGA_TEST,

//input		wire	[17:2]	VME_A,
//inout		wire	[31:0]	VME_D,

inout		[42:0]	MC0_DATA,
inout		[42:0]	MC1_DATA,
output 				MC0_CLK,
output 				MC1_CLK,
output 				MC0_RESET_B,
output 				MC1_RESET_B
);

//------------------------------------------------------------------------------
//	definition of clock & Reset
//------------------------------------------------------------------------------
	wire			RST					;
	wire			SYSCLK				;
	wire			sysDcmLocked		;
	wire			Dcm25Locked			;
	wire			int_ETH_TXCLK		;
	wire			int_clk125			;
	wire			sitcpFifoRe			;
	wire			sitcpFifoEmpty		;

  sysdcm SYSDCM
   (// Clock in ports
    .CLK_IN1(FPGA_CLK),      // IN
    // Clock out ports
    .CLK_OUT1(SYSCLK),     // OUT
    // Status and control signals
    .RESET(~FPGA_RESET_B),// IN
    .LOCKED(sysDcmLocked));      // OUT

   IBUFGDS #(
      .DIFF_TERM("TRUE"), // Differential Termination
      .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE" 
      .IOSTANDARD("LVDS") // Specifies the I/O standard for this buffer
   ) IBUFGDS_inst (
      .O(int_clk125),  // Clock buffer output
      .I(CLK125P),  // Diff_p clock buffer input
      .IB(CLK125N) // Diff_n clock buffer input
   );

  dcm25 DCM25
   (// Clock in ports
    .CLK_IN1(int_clk125),      // IN
    // Clock out ports
    .CLK_OUT1(ETH_CLKIN),     // OUT
    // Status and control signals
    .RESET(~FPGA_RESET_B),// IN
    .LOCKED(Dcm25Locked));      // OUT

/*
  dcm25 DCM25
   (// Clock in ports
    .CLK_IN1_P(CLK125P),    // IN
    .CLK_IN1_N(CLK125N),    // IN
    // Clock out ports
    .CLK_OUT1(int_clk25),     // OUT
    .CLK_OUT2(int_clk125),     // OUT
    // Status and control signals
    .RESET(~FPGA_RESET_B),// IN
    .LOCKED(Dcm25Locked));      // OUT
*/
   assign   RST   = !sysDcmLocked || !Dcm25Locked || !FPGA_RESET_B;
	
	BUFGMUX GMIIMUX(.O(int_ETH_TXCLK), .I0(ETH_TXCLK), .I1(int_clk125), .S(~ETH_1000M_B));

/*
   ODDR ODDR_GTX (
      .Q(ETH_GTXCLK),   // 1-bit DDR output
      .C(int_clk125),   // 1-bit clock input
      .CE(1'b1), // 1-bit clock enable input
      .D1(1'b1), // 1-bit data input (positive edge)
      .D2(1'b0), // 1-bit data input (negative edge)
      .R(1'b0),   // 1-bit reset
      .S(1'b0)    // 1-bit set
   );

   ODDR ODDR_CLKIN (
      .Q(ETH_CLKIN),   // 1-bit DDR output
      .C(int_clk25),   // 1-bit clock input
      .CE(1'b1), // 1-bit clock enable input
      .D1(1'b1), // 1-bit data input (positive edge)
      .D2(1'b0), // 1-bit data input (negative edge)
      .R(1'b0),   // 1-bit reset
      .S(1'b0) 	// 1-bit set
		);
*/
assign ETH_GTXCLK = int_clk125;

//------------------------------------------------------------------------------
//	NETWORK PROTOCOL PROCESSOR (body of SiTCP)
//------------------------------------------------------------------------------
	wire	[15:0]	TCP_RX_WC		;
	wire	[7:0]		TCP_RX_DATA		;
	wire	[7:0]		TCP_TX_DATA		;
	wire				TCP_RX_WR		;
	wire				TCP_TX_WR		;
	wire				TCP_TX_FULL		;

	wire	[31:0]	RBCP_ADDR		;
	wire	[7:0]		RBCP_WD			;
	wire	[7:0]		RBCP_RD			;
	wire				RBCP_WE			;
	wire				RBCP_RE			;
	wire				RBCP_ACK			;

	wire	[31:0]	IP_ADDR			;
	wire	[15:0]	TCP_PORT			;
	wire	[15:0]	RBCP_PORT		;
	
	wire				TCP_OPEN			;
	wire				TCP_CLOSE		;
	wire				TCP_ERROR		;
	
	wire				ETH_MDIO_OE		;
	wire				ETH_MDIO_OUT	;

	assign	ETH_MDIO	= (ETH_MDIO_OE	? ETH_MDIO_OUT	: 1'bz);

	WRAP_SiTCP_GMII_XC7K_32K		
		#(130) // = System clock frequency(MHz), integer only
	SiTCP(
		.CLK						(SYSCLK				),	// in	: System Clock > 129MHz
		.RST						(RST					),	// in	: System reset
	// Configuration parameters
		.FORCE_DEFAULTn		(1'b0					),	// in	: Load default parameters
		.EXT_IP_ADDR			(32'h0				),	// in	: IP address[31:0]
		.EXT_TCP_PORT			(16'd0				),	// in	: TCP port #[15:0]
		.EXT_RBCP_PORT			(16'd0				),	// in	: RBCP port #[15:0]
		.PHY_ADDR				(5'b00001			),	// in	: PHY-device MIF address[4:0]
	// EEPROM
		.EEPROM_CS				(PROM_CS				),	// out	: Chip select
		.EEPROM_SK				(PROM_SK				),	// out	: Serial data clock
		.EEPROM_DI				(PROM_DI				),	// out	: Serial write data
		.EEPROM_DO				(PROM_DO				),	// in	: Serial read data
	// MII interface
		.GMII_RSTn				(ETH_RESET_B			),	// out	: PHY reset
		.GMII_1000M				(~ETH_1000M_B		),	// in	: GMII mode (0:MII, 1:GMII)
		// TX
		.GMII_TX_CLK			(int_ETH_TXCLK	),	// in	: Tx clock
		.GMII_TX_EN				(ETH_TXEN			),	// out	: Tx enable
		.GMII_TXD				(ETH_TXD[7:0]		),	// out	: Tx data[7:0]
		.GMII_TX_ER				(ETH_TXER			),	// out	: TX error
		// RX
		.GMII_RX_CLK			(ETH_RXCLK			),	// in	: Rx clock
		.GMII_RX_DV				(ETH_RXDV			),	// in	: Rx data valid
		.GMII_RXD				(ETH_RXD[7:0]		),	// in	: Rx data[7:0]
		.GMII_RX_ER				(ETH_RXER			),	// in	: Rx error
		.GMII_CRS				(ETH_CRS				),	// in	: Carrier sense
		.GMII_COL				(ETH_COL				),	// in	: Collision detected
		// Management IF
		.GMII_MDC				(ETH_MDC				),	// out	: Clock for MDIO
		.GMII_MDIO_IN			(ETH_MDIO			),	// in	: Data
		.GMII_MDIO_OUT			(ETH_MDIO_OUT		),	// out	: Data
		.GMII_MDIO_OE			(ETH_MDIO_OE		),	// out	: MDIO output enable
	// User I/F
		.SiTCP_RST				(SiTCP_RST			),	// out	: Reset for SiTCP and related circuits
		// TCP connection control
		.TCP_OPEN_REQ			(1'b0					),	// in	: Reserved input, shoud be 0
		.TCP_OPEN_ACK			(TCP_OPEN			),	// out	: Acknowledge for open (=Socket busy)
		.TCP_ERROR				(TCP_ERROR			),	// out	: TCP error, its active period is equal to MSL
		.TCP_CLOSE_REQ			(TCP_CLOSE			),	// out	: Connection close request
		.TCP_CLOSE_ACK			(TCP_CLOSE			),	// in	: Acknowledge for closing
		// FIFO I/F
		.TCP_RX_WC				(TCP_RX_WC[15:0]	),	// in	: Rx FIFO write count[15:0] (Unused bits should be set 1)
		.TCP_RX_WR				(TCP_RX_WR			),	// out	: Write enable
		.TCP_RX_DATA			(TCP_RX_DATA[7:0]	),	// out	: Write data[7:0]
		.TCP_TX_FULL			(TCP_TX_FULL		),	// out	: Almost full flag
		.TCP_TX_WR				(TCP_TX_WR			),	// in	: Write enable
		.TCP_TX_DATA			(TCP_TX_DATA[7:0]	),	// in	: Write data[7:0]
		// RBCP
//		.RBCP_ACT				(RBCP_ACT			),	// out	: RBCP active
		.RBCP_ADDR				(RBCP_ADDR[31:0]	),	// out	: Address[31:0]
		.RBCP_WD					(RBCP_WD[7:0]		),	// out	: Data[7:0]
		.RBCP_WE					(RBCP_WE				),	// out	: Write enable
		.RBCP_RE					(RBCP_RE				),	// out	: Read enable
		.RBCP_ACK				(RBCP_ACK			),	// in	: Access acknowledge
		.RBCP_RD					(RBCP_RD[7:0]		)	// in	: Read data[7:0]
	);

//------------------------------------------------------------------------------
//	TCP loopback FIFO
//------------------------------------------------------------------------------
	assign	TCP_RX_WC[15:0]	= 16'h0;
/*	assign	TCP_RX_WC[15:11]	= 5'b11111;

	loopbackfifo		loopbackfifo(
		.clk			(SYSCLK				),
		.rst			(SiTCP_RST			),
		.data_count	(TCP_RX_WC[10:0]	),
		.full			(						),
		.wr_en		(TCP_RX_WR			),
		.din			(TCP_RX_DATA[7:0]	),
		.empty		(sitcpFifoEmpty	),
		.rd_en		(sitcpFifoRe		),
		.dout			(						),
//		.dout			(TCP_TX_DATA[7:0]	),
		.valid		(						)
//		.valid		(TCP_TX_WR			)
	);
	
	assign	sitcpFifoRe	= ~TCP_TX_FULL & ~sitcpFifoEmpty;
*/
//------------------------------------------------------------------------------
//	RBCP Registers (free registers 0x8-0xf)
//------------------------------------------------------------------------------
	RBCP_REG		RBCP_REG(
		// System
		.CLK					(SYSCLK				),	// in	: System clock
		.RST					(SiTCP_RST			),	// in	: System reset
		// RBCP I/F
//		.RBCP_ACT			(RBCP_ACT			),	// in	: Active
		.RBCP_ADDR			(RBCP_ADDR[31:0]	),	// in	: Address[31:0]
		.RBCP_WE				(RBCP_WE				),	// in	: Write enable
		.RBCP_WD				(RBCP_WD[7:0]		),	// in	: Write data[7:0]
		.RBCP_RE				(RBCP_RE				),	// in	: Read enable
		.RBCP_RD				(RBCP_RD[7:0]		),	// out	: Read data[7:0]
		.RBCP_ACK			(RBCP_ACK			)	// out	: Acknowledge
	);

//**************************** beginnig of main logic ******************************
wire rxclk;
reg [15:0] glinktx = 16'h0;
reg glinktx_flg = 1'b0;
reg [8:0] count = 9'd0;
reg [15:0] glinkrx = 16'hff;
reg [1:0] glinkrx_flg = 2'b00;

//wire rxclk0 = MC1_DATA[40];
wire rxdata = MC1_DATA[39];
wire rxcntl = MC1_DATA[38];
wire rxflag = MC1_DATA[37];
wire rxsd = MC1_DATA[20];
wire rxclk1 = MC1_DATA[17];
wire rxready = MC1_DATA[7];
wire rxerror = MC1_DATA[6];
//wire rxdslip = MC1_DATA[4];
wire rxshfout = MC1_DATA[1];
//wire rxsrqout = MC1_DATA[0];
wire [15:0] rx = {MC1_DATA[36], MC1_DATA[34], MC1_DATA[32:30], MC1_DATA[28:27], MC1_DATA[25],
						MC1_DATA[22:21], MC1_DATA[19], MC1_DATA[16:15], MC1_DATA[13], MC1_DATA[10:9]};
wire txlocked = MC0_DATA[1];

wire rxsrqin = 1'b0;
wire rxwsyncdsb = 1'b0;
wire [1:0] rxdiv = 2'b01;
wire rxpassenb = 1'b0;
wire rxesmpxenb = 1'b0;
wire rxflgenb = 1'b1;
wire rxtstclk = 1'b1;
wire rxshfin = rxshfout;
wire txesmpxenb = 1'b0;
wire txtclkenb = 1'b0;
wire [1:0] txdiv = 2'b01;
wire txflgenb = 1'b1;
wire txdis = 1'b0;
wire txcntl = 1'b0;
wire txdata = glinktx_flg;
wire txflag = 1'b0;
wire [15:0] tx = glinktx[15:0];

assign MC1_DATA[42] = rxsrqin;
assign MC1_DATA[41] = rxwsyncdsb;
assign MC1_DATA[35] = rxdiv[1];
assign MC1_DATA[33] = rxdiv[0];
assign MC1_DATA[29] = rxpassenb;
assign MC1_DATA[26] = rxesmpxenb;
assign MC1_DATA[23] = rxflgenb;
assign MC1_DATA[3] = rxtstclk;
assign MC1_DATA[2] = rxshfin;
assign {MC1_DATA[40:36], MC1_DATA[34], MC1_DATA[32:30], MC1_DATA[28:27], MC1_DATA[25:24],
			MC1_DATA[22:4], MC1_DATA[1:0]} = 34'hz;
assign MC0_DATA[42] = txesmpxenb;
assign MC0_DATA[41] = txtclkenb;
assign MC0_DATA[40] = txdiv[0];
assign MC0_DATA[39] = txdiv[1];
assign MC0_DATA[38] = txflgenb;
assign MC0_DATA[35] = txdis;
assign MC0_DATA[7] = txcntl;
assign MC0_DATA[4] = txdata;
assign MC0_DATA[3] = txflag;
assign {MC0_DATA[9], MC0_DATA[10], MC0_DATA[13], MC0_DATA[15], MC0_DATA[16], MC0_DATA[19],
			MC0_DATA[21], MC0_DATA[22], MC0_DATA[25], MC0_DATA[27], MC0_DATA[28], MC0_DATA[30],
			MC0_DATA[31], MC0_DATA[34], MC0_DATA[36], MC0_DATA[37]} = tx[15:0];
assign {MC0_DATA[33:32], MC0_DATA[29], MC0_DATA[26], MC0_DATA[24:23], MC0_DATA[20],
			MC0_DATA[18:17], MC0_DATA[14], MC0_DATA[12:11], MC0_DATA[8], MC0_DATA[6:5],
			MC0_DATA[2:0]} = 18'hz;

assign FPGA_STATE = txlocked && rxsd && rxready && !rxerror;
assign FPGA_LED = {txlocked, rxsd, rxready, rxerror};

//------------------------------------------------------------------------------
//	Tx data
//------------------------------------------------------------------------------
always @(posedge FPGA_CLK or negedge FPGA_RESET_B) begin
	if (!FPGA_RESET_B) begin
		count <= 9'd0;
		glinktx <= 16'h0;
		glinktx_flg <= 1'b0;
	end
	else if (!txlocked) begin
		count <= 9'd0;
		glinktx <= 16'h0;
		glinktx_flg <= 1'b0;
	end
	else begin
		if (count < 9'd64) begin
			count <= count + 1;
			glinktx <= glinktx + 1;
			glinktx_flg <= 1'b1;
		end
		else if (count < 9'd400) begin
			count <= count + 1;
			glinktx <= glinktx;
			glinktx_flg <= 1'b0;
		end
		else begin
			count <= 9'd0;
			glinktx <= 16'h0;
			glinktx_flg <= 1'b0;
		end
	end
end

//------------------------------------------------------------------------------
//	Rx data
//------------------------------------------------------------------------------
   BUFG BUFG_rx1clk (
      .O(rxclk), // 1-bit output: Clock buffer output
      .I(rxclk1)  // 1-bit input: Clock buffer input
   );

always @(posedge rxclk or negedge FPGA_RESET_B) begin
	if (!FPGA_RESET_B) begin
		glinkrx <= 16'hff;
		glinkrx_flg <= 2'b00;
	end
	else if (!rxsd || !rxready || rxerror) begin
		glinkrx <= 16'hff;
		glinkrx_flg <= 2'b00;
	end
	else if (rxdata || rxcntl) begin
		glinkrx <= rx;
		glinkrx_flg <= {rxcntl, rxdata};
	end
	else begin
		glinkrx <= glinkrx;
		glinkrx_flg <= 2'b00;
	end
end

//------------------------------------------------------------------------------
//	fifo between glink and SiTCP
//------------------------------------------------------------------------------
fifo16_8 sitcpfifo(
  .rst(!FPGA_RESET_B), // input rst
  .wr_clk(rxclk1), // input wr_clk
  .rd_clk(SYSCLK), // input rd_clk
  .din(glinkrx), // input [15 : 0] din
  .wr_en(glinkrx_flg!=2'b00 && TCP_OPEN), // input wr_en
  .rd_en(!TCP_TX_FULL), // input rd_en
  .dout(TCP_TX_DATA), // output [7 : 0] dout
  .full(), // output full
  .wr_ack(), // output wr_ack
  .empty(), // output empty
  .valid(TCP_TX_WR) // output valid
);

   ODDR ODDR_MC0CLK (
      .Q(MC0_CLK),   // 1-bit DDR output
      .C(FPGA_CLK),   // 1-bit clock input
      .CE(1'b1), // 1-bit clock enable input
      .D1(1'b1), // 1-bit data input (positive edge)
      .D2(1'b0), // 1-bit data input (negative edge)
      .R(1'b0),   // 1-bit reset
      .S(1'b0) 	// 1-bit set
		);

   ODDR ODDR_MC1CLK (
      .Q(MC1_CLK),   // 1-bit DDR output
      .C(FPGA_CLK),   // 1-bit clock input
      .CE(1'b1), // 1-bit clock enable input
      .D1(1'b1), // 1-bit data input (positive edge)
      .D2(1'b0), // 1-bit data input (negative edge)
      .R(1'b0),   // 1-bit reset
      .S(1'b0) 	// 1-bit set
		);

assign MC0_RESET_B = FPGA_RESET_B;
assign MC1_RESET_B = FPGA_RESET_B;

assign FPGA_TEST = {txcntl, txdata, tx[5:0], rxcntl, rxdata, rx[5:0]};

//**************************** end of main logic ******************************

endmodule
